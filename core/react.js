/* ----------------------------------------
	This file controls all the events fired
	off by reactions from users, i.e.
	a thumbs up or bug tag
 ---------------------------------------- */
var exports = module.exports = {};

const unicode = require("emoji-unicode-map");

const logger 	= require("./logger"),
	dio			= require("./dio"),
	x 			= require("./vars"),
	helpers		= require("./helpers");

exports.onReact = function(bot, fire, e, remove = false) {
	// Get emoji and stuff, then log it
	const emoji = (unicode.get(e.d.emoji.name)) ? unicode.get(e.d.emoji.name) : e.d.emoji.name,
		channelID = e.d["channel_id"],
		userID = e.d.user_id,
		msgID = e.d["message_id"];

	logger.log(`${userID} reacted with ${emoji} | ${e.d.emoji.name}`);

	// Read the message that was reacted to
	bot.getMessage({
		channelID: channelID,
		messageID: msgID
	},(err, msg)=> {
		// Error out
		if (err) {
			logger.log(err, "Error");
			return false;
		}

		// If we returned a message successfully
		if (msg && msg.hasOwnProperty("author")) {
			const author = (msg.author.id) ? msg.author.id : null,
				eRoles = (bot.servers[x.chan].members[userID].hasOwnProperty("roles")) ? bot.servers[x.chan].members[userID].roles : [];

			// Check for a message by the bot
			if (author === bot.id) {
				// If reacting to an embed
				if (msg.embeds && msg.embeds[0] && msg.embeds[0].author) {
					// Check for a balance embed
					if (msg.embeds[0].author.name === "Proposed Balance Change") {
						fire.balance.child("newchanges").child(msg.embeds[0].footer.text).update({
							reactions: msg.reactions
						});
					}

					// Check for a language embed
					if (msg.embeds[0].author.name.startsWith("Language Detected")) {
						const chanID = msg.embeds[0].fields[0].value,
							userID = msg.embeds[0].fields[1].value,
							data = {bot: bot, db: fire, channelID: x.modchan};

						let langWarnings = [
							`<@${userID}>, I'd appreciate if you didn't use that sort of language in this community. I'm sure you will find more suitable vocabulary to express your thoughts clearly.`,
							`Hey <@${userID}>, can we avoid that sort of language here? There's a slew of other ways to express yourself, I'm sure!`,
							`<@${userID}>, let's not speak like that here, please. Feel free to express yourself in more appropriate words!`,
							`Easy there <@${userID}>, let's not use that kind of language in this community. There's probably a better way to phrase that.`,
						];

						if (emoji === "+1" || e.d.emoji.name.startsWith("👍")) { // Warn
							dio.del(msg.id, data);

							let n = Math.floor( Math.random()*langWarnings.length );
							dio.say(langWarnings[n], data, chanID);

							helpers.modEmbed(data, {
								admin: "Pocketbot",
								action: "warn",
								icon: ":warning:",
								user: userID,
								msg: msg.embeds[0].description.split("said...")[1].replace("\n Should I issue a warning? \n ___",""),
								chanID: chanID
							});
						}

						if (emoji === "-1" || e.d.emoji.name.startsWith("👎")) { // Dismiss
							dio.del(msg.id, data);
						}
					}

					// Check for warning embed
					if (msg.embeds[0].author.name.startsWith("Issuing Warning")) {
						const chanID = msg.embeds[0].fields[0].value,
							p = ["folks", "everyone", "everybody", "peeps", "people", "peoples", "peasants", "crew", "y'all"],
							user = (msg.embeds[0].fields[1]) ? `<@${msg.embeds[0].fields[1].value}>` : p[Math.floor( Math.random()*p.length )],
							data = {bot: bot, db: fire, channelID: chanID},
							// Add some more sets of warnings
							langWarnings = [ // Gotta repeat this one to get a different var in here >_>
								`Hey ${user}, I'd appreciate if you didn't use that sort of language in this community. I'm sure you will find more suitable vocabulary to express your thoughts clearly.`,
								`Hey ${user}, can we avoid that sort of language here? There's a slew of other ways to express yourself, I'm sure!`,
								`Excuse me ${user}, let's not speak like that here, please. Feel free to express yourself in more appropriate words!`,
								`Easy there ${user}, let's not use that kind of language in this community. There's probably a better way to phrase that.`,
							],
							spamWarnings = [
								`Take it easy on the spam ${user}.`,
								`Hey ${user}, simmer down with all those keys please.`,
								`Oy ${user}, easy on the keyboard.`,
								`Calm down ${user}, no one likes the spam.`
							],
							behWarnings = [
								`I'd appreciate if you adjusted your behavior, ${user}.`,
								`C'mon ${user}, let's keep things proper around here.`,
								`That is not how to conduct yourself here ${user}, chill.`,
								`Calm down ${user}, let's keep things **civilized** around here.`
							];

						let warned = false;

						switch (e.d.emoji.name) {
						case "🙊": // Speak Monkey
							dio.say(langWarnings[ Math.floor( Math.random()*langWarnings.length ) ], data, chanID);
							warned = true;
							break;
						case "🤐": // Zipper mouth
							dio.say(spamWarnings[ Math.floor( Math.random()*spamWarnings.length ) ], data, chanID);
							warned = true;
							break;
						case "💊": // Pill
							dio.say(behWarnings[ Math.floor( Math.random()*spamWarnings.length ) ], data, chanID);
							warned = true;
							break;
						}

						if (warned) {
							helpers.modEmbed(data, {
								admin: "Pocketbot",
								action: "warn",
								icon: ":warning:",
								user: (msg.embeds[0].fields[1]) ? msg.embeds[0].fields[1].value : "ALL",
								msg: "[General warning]",
								chanID: chanID
							});
						}

						dio.del(msg.id, data);
					}
				}
			}

			// Check for a bug report (admin/mod, bug emoji, in #troubleshooting)
			if ((eRoles.includes(x.admin) || eRoles.includes(x.mod)) && emoji === "bug" && channelID === x.trouble) {
				// If adding the emote, let's log it!
				if (!remove) try {
					fire.bugs.push({
						content: msg.content,
						attachments: msg.attachments,
						timestamp: msg.timestamp,
						uid: msg.author.username
					});
				} catch(e) { logger.log(e, "Error"); }
			}
		}	
	});
};
/* ----------------------------------------
	This file contains most of the general
	commands (formerly in Mastabot) that the
	public community uses.
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	dio		= require("../core/dio"),
	x 		= require("../core/vars");

let cmdAlpha = new command("community", "!alpha", "Gives information about joining the alpha", function(data) {
	dio.del(data.messageID, data);
	dio.say(`:pencil: Learn more about the alpha in the <#${x.rules}> channel. :thumbsup:`,data);
});

let cmdControls = new command("community", "!controls", "Display game controls", function(data) {
	dio.say(":video_game: These are the current controls: http://pocketwatchgames.com/presskit/Tooth%20and%20Tail/images/controllercscreen.png",data);
});

let cmdBugs = new command("community", "!bugs", "Link to the troubleshooting channel", function(data) {
	dio.say(":beetle: Report bugs here: <#134810918113509376>",data);
});

let cmdWiki = new command("community", "!wiki", "Link to Freakspot's TnT Wiki", function(data) {
	dio.say("Check out the wiki: http://toothandtailwiki.com/",data);
});

let cmdTourney = new command("community", "!tournament", "Link to the Clash of Comrades website", function(data) {
	dio.say("For Tournament and game info, VODs, and other community events, check out http://www.clashofcomrades.com",data);
});

let cmdRoadmap = new command("community", "!roadmap", "Links to public roadmap", function(data) {
	dio.del(data.messageID,data);
	dio.say(":map: View the public roadmap here: <https://trello.com/b/N8EzNonZ/tooth-and-tail-roadmap> \n\n If you wish to comment or add something (into the **Guest Requests** only), sign up through here: <https://trello.com/invite/b/N8EzNonZ/4888ec2b4b67b55825dea3ad939b5bc1/tooth-and-tail-roadmap>", data);
});

let cmdKillDancer = new command("community", "!killdancer", "Kills any dancers Hox posts", function(data) {

	if (data.dance != "") {
		logger.log(data.dance, logger.MESSAGE_TYPE.Info);
		dio.del(data.messageID, data);
		dio.del(data.dance, data);
		let weapons = [
			":knife::astonished:",
			":scream::gun:",
			":dagger::astonished:",
			":bomb: :skull:"
		];
		dio.say(weapons[Math.floor(Math.random()*weapons.length)], data);
	}
});

cmdKillDancer.permissions = [x.mod, x.admin];

let cmd8Ball = new command("community", "!8ball", "Asks the 8ball a question", function(data) {

	let x = Math.floor(Math.random()*(20)-1),
		answer = [
			"It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
			"As I see it, yes", "Most likely", "Outlook good", "Yes",
			"Signs point to yes", "Reply hazy try again", "Ask again later", "Better not tell you now",
			"Cannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no",
			"My sources say no", "Outlook not so good", "Very doubtful"
		];

	dio.say(`:8ball: says _"${answer[x]}"_`,data);
});

let cmdRegion = new command("community", "!region", "Allows user to set region roles on themselves", function(data) {
	if (data.args[1]) {
		let region = data.args[1].toLowerCase();

		switch(region) {
		case "na":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.na
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		case "eu":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.eu
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		case "au":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.au
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		default:
			dio.say("🕑 You didn't specify a region. Please choose one of the following: `NA | EU | AU`",data);
			break;
		}
	} else {
		dio.say("🕑 You didn't specify a region. Please choose one of the following: `NA | EU | AU`",data);
	}
});

/*
let cmdLegend = new command("community", "!legend", "Gives the unit/code legend for registering", function(data) {
	dio.del(data.messageID, data);
	dio.say(`<@${data.userID}>, the legend is:
:zero:  ${x.emojis.pigeon}
:one:  ${x.emojis.squirrel}
:two:  ${x.emojis.lizard}
:three:  ${x.emojis.toad}
:four:  ${x.emojis.mole}
:five:  ${x.emojis.falcon}
:six:  ${x.emojis.ferret}
:seven:  ${x.emojis.skunk}
:eight:  ${x.emojis.snake}
:nine:  ${x.emojis.chameleon}
Hover over each emoji to find out how to type them.`,data, x.playground);
});

let cmdRegister = new command("community", "!register", "Registers a user as a recruit", (data) => {
	// Check that user has no roles
	if (data.bot.servers[x.chan].members[data.userID].roles.length != 0) {
		dio.del(data.messageID, data);
		dio.say("You're already a standard user. If you'd like to _lose_ your current role, please see if Masta will ban you.",data);
	} else {
		// Get ID Pin
		let rawpin = data.message.replace("!register","").replace(/\s/g,""),
			realpin = data.userID.slice(0,4),
			// Replace & Compare
			temppin = "";

		for (let i=0, len = realpin.length; i<len; i++) {
			switch (realpin[i]) {
			case "0":
				temppin += `${x.emojis.pigeon}`;
				break;
			case "1":
				temppin += `${x.emojis.squirrel}`;
				break;
			case "2":
				temppin += `${x.emojis.lizard}`;
				break;
			case "3":
				temppin += `${x.emojis.toad}`;
				break;
			case "4":
				temppin += `${x.emojis.mole}`;
				break;
			case "5":
				temppin += `${x.emojis.falcon}`;
				break;
			case "6":
				temppin += `${x.emojis.ferret}`;
				break;
			case "7":
				temppin += `${x.emojis.skunk}`;
				break;
			case "8":
				temppin += `${x.emojis.snake}`;
				break;
			case "9":
				temppin += `${x.emojis.chameleon}`;
				break;
			}
		}

		if (temppin === rawpin) {
			// Add them to Recruit
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.noob
			}, (err, res) => {
				if (err) logger.log(`Aww frickin': ${err} | ${res}`,"Error");
				dio.say(":tada: You have been successfully registered!", data);
				//dio.say(`:mailbox: <@${data.userID}> is now a Recruit.`, data, x.history);
			});
		} else {
			dio.say("🕑 You seem to have mistyped your code. Use the emoji list to select the units you need, use the `!legend` command if you need to know which unit represents which number, and check the message I first PMed you for your ID number.",data);
		}
	}
});
*/

module.exports.commands = [cmdRoadmap, cmdTourney, cmdWiki, cmdBugs, cmdControls, cmdAlpha, cmdKillDancer, cmd8Ball, cmdRegion];

function emoteCmd(data, arr, e=false, ext=".gif") {
	dio.del(data.messageID, data);
	let key = data.args[0].replace(":","").replace(":",""), // Can't do aliases in debug like this
		emote = (e) ? e : arr[key],
		uRoles = data.bot.servers[x.chan].members[data.userID].roles;

	// Mods/Admins just go
	if (uRoles.includes(x.mod) || uRoles.includes(x.admin)) {
		dio.sendImage(`./emoji/${emote}${ext}`, data); // This could be handled better but this gets the job done
		logger.log(`Uploaded ${emote}${ext}`, "OK");
	} else {
		// Check everyone else
		data.userdata.getProp(data.userID, "emotes").then( (res) => {
			if ( res && res.includes(emote) ) {
				dio.sendImage(`./emoji/${emote}${ext}`, data);
				logger.log(`Uploaded ${emote}${ext}`, "OK");
			} else {
				dio.say("🕑 You have not purchased this emote. You should check the `!shop` for more information." , data);
			}
		});
	}
}

for (let key in x.gifaliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the animated "+x.aliases[key]+" emote", function(data) {
		emoteCmd(data, x.gifaliases);
	}));
}

for (let key in x.aliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the "+x.aliases[key]+" emote!", function(data) {
		emoteCmd(data, x.aliases, false, ".png");
	}));
	module.exports.commands[module.exports.commands.length-1].triggerType = 0;
}

x.gifemotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the animated "+emote+" emote!", function(data) {
		emoteCmd(data, false, emote);
	}));
});

x.emotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the "+emote+" emote", function(data) {
		emoteCmd(data, false, emote, ".png");
	}));
});
